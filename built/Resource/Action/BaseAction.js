"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseAction = void 0;
/**
 * Abstract Class BaseAction
 *
 * @class BaseAction
 */
var BaseAction = /** @class */ (function () {
    function BaseAction() {
        if (this.constructor == BaseAction) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }
    return BaseAction;
}());
exports.BaseAction = BaseAction;
