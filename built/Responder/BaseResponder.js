"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseResponder = void 0;
/**
 * Abstract Class BaseResponder
 *
 * @class BaseResponder
 */
var BaseResponder = /** @class */ (function () {
    function BaseResponder() {
        if (this.constructor == BaseResponder) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }
    return BaseResponder;
}());
exports.BaseResponder = BaseResponder;
