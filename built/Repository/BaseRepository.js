"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRepository = void 0;
/**
 * Abstract Class BaseRepository
 *
 * @class BaseRepository
 */
var BaseRepository = /** @class */ (function () {
    function BaseRepository() {
        if (this.constructor == BaseRepository) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }
    return BaseRepository;
}());
exports.BaseRepository = BaseRepository;
