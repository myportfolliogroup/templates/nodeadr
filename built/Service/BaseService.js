"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseService = void 0;
/**
 * Abstract Class BaseService
 *
 * @class BaseService
 */
var inversify_1 = require("inversify");
var BaseService = /** @class */ (function () {
    function BaseService() {
        if (this.constructor == BaseService_1) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }
    BaseService_1 = BaseService;
    var BaseService_1;
    BaseService = BaseService_1 = __decorate([
        inversify_1.injectable(),
        __metadata("design:paramtypes", [])
    ], BaseService);
    return BaseService;
}());
exports.BaseService = BaseService;
