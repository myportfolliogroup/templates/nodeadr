"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDomain = void 0;
/**
 * Abstract Class BaseDomain
 *
 * @class BaseDomain
 */
var BaseDomain = /** @class */ (function () {
    function BaseDomain() {
        if (this.constructor == BaseDomain) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }
    return BaseDomain;
}());
exports.BaseDomain = BaseDomain;
