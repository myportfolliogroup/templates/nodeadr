/**
 * Abstract Class BaseResponder
 *
 * @class BaseResponder
 */
export class BaseResponder {
    constructor() {
        if (this.constructor == BaseResponder) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }
}