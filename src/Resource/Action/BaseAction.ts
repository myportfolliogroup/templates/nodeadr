/**
 * Abstract Class BaseAction
 *
 * @class BaseAction
 */
export class BaseAction {
    constructor() {
        if (this.constructor == BaseAction) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }
}