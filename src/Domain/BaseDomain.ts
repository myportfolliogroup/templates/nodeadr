/**
 * Abstract Class BaseDomain
 *
 * @class BaseDomain
 */
export class BaseDomain {
    constructor() {
        if (this.constructor == BaseDomain) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }

}