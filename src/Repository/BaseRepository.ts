/**
 * Abstract Class BaseRepository
 *
 * @class BaseRepository
 */
export class BaseRepository {
    constructor() {
        if (this.constructor == BaseRepository) {
            throw new Error("Abstract classes can't be instantiated.");
        }
    }

}