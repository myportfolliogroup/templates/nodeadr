/**
 * Abstract Class BaseService
 *
 * @class BaseService
 */
import {injectable} from "inversify";

@injectable()
export class BaseService {
    constructor() {
        if (this.constructor == BaseService) {
          throw new Error("Abstract classes can't be instantiated.");
        }
    }
}